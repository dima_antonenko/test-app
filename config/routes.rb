Rails.application.routes.draw do

  root 'site/pages#home'

  devise_for :users
  get 'users/dashboard', as: 'user_root'

  namespace :users do
    resources :posts, except: [:show]
  end

  scope module: 'site' do
    resources :posts,  only: [:show]
  end
end