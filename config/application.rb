require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TestApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2


    config.autoload_paths += Dir[
        "#{config.root}/lib/**/",
        "#{config.root}/app/facades/**/",
        "#{config.root}/app/exceptions",
        "#{config.root}/app/policies/**/"
    ]
  end
end
