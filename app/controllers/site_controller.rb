class SiteController < ApplicationController

  layout 'site'
  before_action :all_pages_data

  private

  # информация которая будет на всех страницах
  def all_pages_data; end
end