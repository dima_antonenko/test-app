class Users::PostsController < UsersController

  before_action :get_post, only: [:edit, :update, :destroy]

  def index
    @posts = Post.all
  end

  def edit; end

  def update
    @post.update_attributes(post_params)
    redirect_back(fallback_location: user_root_path)
  end

  def destroy
    @post.destroy
    redirect_back(fallback_location: user_root_path)
  end

  def new
    @post = Post.new
  end

  def create
    post = Post.new
    post.assign_attributes(post_params)
    if post.save
      redirect_to edit_users_post_path(post)
    else
      redirect_back(fallback_location: user_root_path)
    end
  end

  private

  def get_post
    @post = Post.find(params[:id])
  end


  def post_params
    params.require(:post).permit(:title, :description, :meta_title, :meta_description, :meta_keywords)
  end
end