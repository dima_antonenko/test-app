class UserPolicy < ApplicationPolicy

  attr_accessor(:user)

  def initialize(user)
    @user = user
  end

  def access_to_admin_panel?
    user.is_admin
  end
end