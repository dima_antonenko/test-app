class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title, index: true
      t.text :description, index: true
      t.string :descriptor, index: true

      t.string :meta_title, index: true
      t.string :meta_description, index: true
      t.string :meta_keywords, index: true
    end
  end
end
