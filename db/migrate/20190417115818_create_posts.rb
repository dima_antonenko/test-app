class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    drop_table :pages

    create_table :posts do |t|
      t.string :title, index: true
      t.text :description, index: true

      t.string :meta_title, index: true
      t.text :meta_description, index: true
      t.string :meta_keywords, index: true
      t.timestamps
    end
  end
end
